package com.example.evaluationperuapps.data.repository

import com.example.evaluationperuapps.data.api.RemoteDataSource
import com.example.evaluationperuapps.data.entities.MovieEntity
import com.example.evaluationperuapps.data.entities.UserEntity
import com.example.evaluationperuapps.domain.entities.Movie
import com.example.evaluationperuapps.domain.entities.User
import com.example.evaluationperuapps.domain.mapper.MovieEntityToMovieMapper
import com.example.evaluationperuapps.domain.mapper.UserEntityToUserUIMapper
import io.reactivex.Single


class RepositoryImpl(private var dataSource: RemoteDataSource) : Repository {

    lateinit var moviesMapper: MovieEntityToMovieMapper

    lateinit var userMapperEntity: UserEntityToUserUIMapper

    override fun login(email: String, pass: String): Single<UserEntity> =
        dataSource.fetchLogin(email, pass)
    //   dataSource.fetchLogin(email, pass).map { userMapperEntity.map(it) }

    override fun getListMovies(token: String): Single<MovieEntity> =
        dataSource.fetchMovies(token)
    //    dataSource.fetchMovies(token).map { moviesMapper.mapList(it) }
}