package com.example.evaluationperuapps.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserEntity : BaseEntity(){
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("data")
    @Expose
    var dataEntity: DataEntity? = null
}