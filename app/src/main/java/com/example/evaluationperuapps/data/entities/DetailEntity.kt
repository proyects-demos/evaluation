package com.example.evaluationperuapps.data.entities

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DetailEntity : Serializable{
    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("img")
    @Expose
    var img: String? = null
}