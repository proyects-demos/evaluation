package com.example.evaluationperuapps.data.entities

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class DataEntity {
    @SerializedName("token")
    @Expose
    var token: String? = null
}