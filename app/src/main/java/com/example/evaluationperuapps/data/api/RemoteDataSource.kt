package com.example.evaluationperuapps.data.api

import com.example.evaluationperuapps.data.entities.MovieEntity
import com.example.evaluationperuapps.data.entities.UserEntity
import io.reactivex.Single


interface RemoteDataSource {
    fun fetchMovies(token: String): Single<MovieEntity>
    fun fetchLogin(email: String, pass: String): Single<UserEntity>
}