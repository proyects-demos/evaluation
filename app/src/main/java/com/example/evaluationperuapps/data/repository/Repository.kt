package com.example.evaluationperuapps.data.repository

import com.example.evaluationperuapps.data.entities.MovieEntity
import com.example.evaluationperuapps.data.entities.UserEntity
import com.example.evaluationperuapps.domain.entities.Movie
import com.example.evaluationperuapps.domain.entities.User
import io.reactivex.Single



interface Repository {
    fun login(email: String, pass: String): Single<UserEntity>
    fun getListMovies(token: String): Single<MovieEntity>
}