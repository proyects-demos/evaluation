package com.example.evaluationperuapps.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseEntity {
    @SerializedName("message")
    @Expose
    var message: String? = null
}