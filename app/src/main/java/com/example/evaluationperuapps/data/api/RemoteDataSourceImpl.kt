package com.example.evaluationperuapps.data.api

import com.example.evaluationperuapps.BuildConfig
import com.example.evaluationperuapps.data.api.request.BaseRequest
import com.example.evaluationperuapps.data.entities.UserEntity
import com.example.evaluationperuapps.data.entities.MovieEntity
import com.example.evaluationperuapps.presentation.common.utils.Constants.BEARER
import io.reactivex.Single


class RemoteDataSourceImpl(private val apiService: ApiService) : RemoteDataSource {

    override fun fetchMovies(token: String): Single<MovieEntity> =
        apiService.getMovies(BEARER + token)


    override fun fetchLogin(email: String, pass: String): Single<UserEntity> =
        apiService.login(getParams(email, pass))


    private fun getParams(email: String, pass: String): BaseRequest {
        val request = BaseRequest()
        request.email = email
        request.password = pass
        return request
    }
}