package com.example.evaluationperuapps.data.api

import com.example.evaluationperuapps.data.api.request.BaseRequest
import com.example.evaluationperuapps.data.entities.UserEntity
import com.example.evaluationperuapps.data.entities.MovieEntity
import com.example.evaluationperuapps.presentation.common.utils.Constants
import io.reactivex.Single

import retrofit2.http.*


interface ApiService {

    @Headers(Constants.CONTENT_TYPE)
    @POST("auth/login")
    fun login(@Body params: BaseRequest): Single<UserEntity>

    @Headers(Constants.CONTENT_TYPE)
    @GET("movies")
    fun getMovies(
        @Header(value = Constants.AUTHORIZATION) token: String
    ): Single<MovieEntity>
}