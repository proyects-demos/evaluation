package com.example.evaluationperuapps.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MovieEntity : BaseEntity() {
    @SerializedName("data")
    @Expose
    var datumEntities: List<DatumEntity>? = null

    @SerializedName("links")
    @Expose
    var linksEntity: LinksEntity? = null

    @SerializedName("meta")
    @Expose
    var metaEntity: MetaEntity? = null

    @SerializedName("status")
    @Expose
    var status: Int? = null
}