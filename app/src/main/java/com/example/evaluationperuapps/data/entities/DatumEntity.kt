package com.example.evaluationperuapps.data.entities

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class DatumEntity {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("detail")
    @Expose
    var detailEntity: DetailEntity? = null
}