package com.example.evaluationperuapps.presentation.common.utils

object Constants {
    const val AUTHORIZATION = "Authorization"
    const val BEARER = "Bearer "
    const val CONTENT_TYPE = "Content-Type: application/json"
    const val ESTATUS_OK = 201
    const val EXTRA_TOKEN = "token"
    const val EXTRA_DETAIL = "detail"
    const val ESTATUS_200 = 200


}