package com.example.evaluationperuapps.presentation.mapper
import com.example.evaluationperuapps.domain.entities.Links
import com.example.evaluationperuapps.presentation.entities.LinksUI

object LinksToLinksUI : Mapper<Links, LinksUI>(){
    override fun map(item: Links): LinksUI {
        val linksUI = LinksUI()
        linksUI.first = item.first
        linksUI.last = item.last
        linksUI.next = item.next
        linksUI.prev = item.prev
        return linksUI

    }

    override fun reverseMap(obj: LinksUI): Links {
        val links = Links()
        links.first = obj.first
        links.last = obj.last
        links.next = obj.next
        links.prev = obj.prev
        return links
    }
}