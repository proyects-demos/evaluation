package com.example.evaluationperuapps.presentation.mapper

import com.example.evaluationperuapps.domain.entities.Movie
import com.example.evaluationperuapps.presentation.entities.MovieUI

class MovieToMovieUIMapper : Mapper<Movie, MovieUI>() {
    override fun map(item: Movie): MovieUI {
        val movieUI = MovieUI()

        return movieUI

    }

    override fun reverseMap(obj: MovieUI): Movie {
        val movie = Movie()
        return movie
    }


}