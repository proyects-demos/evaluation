package com.example.evaluationperuapps.presentation.ui.views

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.Coil
import coil.request.CachePolicy
import coil.request.LoadRequest
import coil.transform.CircleCropTransformation
import com.example.evaluationperuapps.data.entities.DatumEntity
import com.example.evaluationperuapps.presentation.ui.adapter.MoviesAdapter


object HomeBinding {

    @BindingAdapter("app:items")
    @JvmStatic
    fun setItems(recyclerView: RecyclerView, items: List<DatumEntity>) {
        with(recyclerView.adapter as MoviesAdapter) {
            setData(items)
        }
    }

    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, url: String?) {

        val imageLoader = Coil.imageLoader(view.context)
        val request = LoadRequest.Builder(view.context)
            .data(url)
            .memoryCachePolicy(CachePolicy.WRITE_ONLY)
            .target(view)
            .transformations(CircleCropTransformation())
            .build()
        imageLoader.execute(request)
        /*
        view.load(url) {
            transformations(CircleCropTransformation())
        }

         */
    }

}