package com.example.evaluationperuapps.presentation.mapper

import com.example.evaluationperuapps.domain.entities.Datum
import com.example.evaluationperuapps.presentation.entities.DatumUI

object DatumToDatumUI : Mapper<Datum, DatumUI>() {
    override fun map(item: Datum): DatumUI {
        val datumUI = DatumUI()
        datumUI.id = item.id
        datumUI.detailEntity = item.detail?.let { DetailToDetailUIMapper.map(it) }
        return datumUI

    }

    override fun reverseMap(obj: DatumUI): Datum {
        val datum = Datum()
        datum.id = obj.id
        datum.detail = obj.detailEntity?.let { DetailToDetailUIMapper.reverseMap(it) }
        return datum
    }
}