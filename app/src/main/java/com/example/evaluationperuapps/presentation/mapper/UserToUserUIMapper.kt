package com.example.evaluationperuapps.presentation.mapper

import com.example.evaluationperuapps.domain.entities.User
import com.example.evaluationperuapps.domain.mapper.DataEntityToData
import com.example.evaluationperuapps.presentation.entities.UserUI

class UserToUserUIMapper : Mapper<User, UserUI>() {
    override fun map(item: User): UserUI {
        val loginUI = UserUI()
        loginUI.status = item.status
        loginUI.data = item.data?.let { DataEntityToData.reverseMap(it) }
        return loginUI

    }

    override fun reverseMap(obj: UserUI): User {
        val login = User()
        return login
    }
}