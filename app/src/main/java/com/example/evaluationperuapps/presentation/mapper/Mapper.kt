package com.example.evaluationperuapps.presentation.mapper

import java.util.*

abstract class Mapper<K, V> {

    abstract fun map(item: K): V

    abstract fun reverseMap(obj: V): K

    open fun mapList(list: List<K>): List<V>? {
        val returnList: MutableList<V> = ArrayList()
        for (`object` in list) {
            returnList.add(map(`object`))
        }
        return returnList
    }

    open fun reverseMapList(list: List<V>): List<K>? {
        val returnList: MutableList<K> = ArrayList()
        for (obj in list) {
            returnList.add(reverseMap(obj))
        }
        return returnList
    }

}