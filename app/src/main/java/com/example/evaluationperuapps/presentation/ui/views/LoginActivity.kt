package com.example.evaluationperuapps.presentation.ui.views

import android.content.Intent
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.evaluationperuapps.R
import com.example.evaluationperuapps.databinding.ActivityLoginBinding
import com.example.evaluationperuapps.presentation.common.base.BaseActivity
import com.example.evaluationperuapps.presentation.common.utils.Constants.EXTRA_TOKEN
import com.example.evaluationperuapps.presentation.ui.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginActivity : BaseActivity<ActivityLoginBinding>() {

    private val viewModel: LoginViewModel by viewModel()


    override fun setLayout(): Int {
        return R.layout.activity_login
    }

    override fun setupView() {
        binding.viewmodel = viewModel
        setupToast()
        setupGoToMovies()
    }

    private fun setupToast() {
        viewModel.toast.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun setupGoToMovies() {
        viewModel.token.observe(this, Observer {
            val extras = Intent()
            extras.putExtra(EXTRA_TOKEN, it.token)
            navigateActivity(this, MoviesActivity::class.java, extras)
        })
    }


}