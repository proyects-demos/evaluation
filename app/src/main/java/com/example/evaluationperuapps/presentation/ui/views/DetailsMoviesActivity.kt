package com.example.evaluationperuapps.presentation.ui.views
import com.example.evaluationperuapps.R
import com.example.evaluationperuapps.data.entities.DetailEntity
import com.example.evaluationperuapps.databinding.ActivityDetailsMoviesBinding
import com.example.evaluationperuapps.presentation.common.base.BaseActivity
import com.example.evaluationperuapps.presentation.common.utils.Constants
import com.example.evaluationperuapps.presentation.ui.viewmodel.MoviesDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailsMoviesActivity : BaseActivity<ActivityDetailsMoviesBinding>() {
    private val viewModel: MoviesDetailViewModel by viewModel()

    override fun setLayout(): Int {
        return R.layout.activity_details_movies
    }

    override fun setupView() {
        binding.viewmodel = viewModel
        getExtrasFromIntent()
    }

    private fun getExtrasFromIntent() {
        val detailEntity = intent.getSerializableExtra(Constants.EXTRA_DETAIL) as DetailEntity
        viewModel.startViewModel(detailEntity)
    }


}