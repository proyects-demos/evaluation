package com.example.evaluationperuapps.presentation.mapper


import com.example.evaluationperuapps.domain.entities.Meta
import com.example.evaluationperuapps.presentation.entities.MetaUI

object MetaToMetaUI : Mapper<Meta, MetaUI>() {
    override fun map(item: Meta): MetaUI {
        val metaUI = MetaUI()
        metaUI.currentPage = item.currentPage
        metaUI.from = item.from
        metaUI.lastPage = item.lastPage
        metaUI.path = item.path
        metaUI.perPage = item.perPage
        metaUI.to = item.to
        metaUI.total = item.total
        return metaUI
    }

    override fun reverseMap(obj: MetaUI): Meta {
        val meta = Meta()
        meta.currentPage = obj.currentPage
        meta.from = obj.from
        meta.lastPage = obj.lastPage
        meta.path = obj.path
        meta.perPage = obj.perPage
        meta.to = obj.to
        meta.total = obj.total
        return meta
    }
}