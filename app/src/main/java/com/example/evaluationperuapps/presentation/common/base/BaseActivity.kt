package com.example.evaluationperuapps.presentation.common.base

import android.R
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import kotlinx.android.synthetic.main.toolbar.view.*

abstract class BaseActivity<D : ViewDataBinding> : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, setLayout())
        binding.lifecycleOwner = this
        setupView()
        getToolbar()
    }

    protected lateinit var binding: D

    protected abstract fun setLayout(): Int

    protected abstract fun setupView()


    protected open fun getToolbar(): Toolbar? {
        val mToolbar: Toolbar? = binding.root.toolbar
        setSupportActionBar(mToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return mToolbar
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun navigateActivity(context: Context, goActivity: Class<*>, extras: Intent) {
        startActivity(
            Intent(context, goActivity).putExtras(extras)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        )
    }
}