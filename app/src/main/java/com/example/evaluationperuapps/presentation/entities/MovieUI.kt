package com.example.evaluationperuapps.presentation.entities

import com.example.evaluationperuapps.data.entities.DatumEntity
import com.example.evaluationperuapps.data.entities.LinksEntity
import com.example.evaluationperuapps.data.entities.MetaEntity


class MovieUI {
    var datumUI: List<DatumUI>? = null
    var linksUI: LinksUI? = null
    var metaUI: MetaUI? = null
    var status: Int? = null
}