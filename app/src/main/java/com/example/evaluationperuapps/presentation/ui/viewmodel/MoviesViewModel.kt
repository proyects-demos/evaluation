package com.example.evaluationperuapps.presentation.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.example.evaluationperuapps.R
import com.example.evaluationperuapps.data.entities.DatumEntity
import com.example.evaluationperuapps.data.entities.DetailEntity
import com.example.evaluationperuapps.domain.usecases.MoviesUseCase
import com.example.evaluationperuapps.presentation.common.base.BaseViewModel
import com.example.evaluationperuapps.presentation.common.utils.Constants
import com.example.evaluationperuapps.presentation.entities.MovieUI
import com.example.evaluationperuapps.presentation.mapper.MovieToMovieUIMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MoviesViewModel(private val moviesUseCase: MoviesUseCase) : BaseViewModel() {

    private var dataResponse: LiveData<List<MovieUI>> = MutableLiveData()

    private val movieToMoviesUiMapper = MovieToMovieUIMapper()

    private val _token = MediatorLiveData<String>()

    val key: LiveData<String> get() = _token

    private val _message = MutableLiveData<String>()
    val toast: LiveData<String> = _message

    private val _movies = MediatorLiveData<List<DatumEntity>>().apply { value = emptyList() }

    val movies: LiveData<List<DatumEntity>> get() = _movies


    private val _detailMovie = MediatorLiveData<DetailEntity>()
    val detail: LiveData<DetailEntity> = _detailMovie

    fun movieClicksOnItem(detailEntity: DetailEntity) {
        _detailMovie.value = detailEntity
    }

    fun startViewModel(token: String) {
        _token.value = token
        getAllMovies()
    }

    private fun getAllMovies() {
        val disposable = moviesUseCase.getMovies(token = _token.value ?: "")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            //   .map(movieToMoviesUiMapper::mapList)
            .subscribe({ response ->
                if (response.status == Constants.ESTATUS_200) {
                    _movies.value = response.datumEntities
                } else {

                    _message.value = response.message
                }
            }, {
                _message.value = String.format("", R.string.no_connection)
            })
        this.disposable.add(disposable)

    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}