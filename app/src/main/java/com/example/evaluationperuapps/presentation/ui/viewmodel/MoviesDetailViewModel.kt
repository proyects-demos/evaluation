package com.example.evaluationperuapps.presentation.ui.viewmodel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.evaluationperuapps.data.entities.DetailEntity
import com.example.evaluationperuapps.presentation.common.base.BaseViewModel

class MoviesDetailViewModel : BaseViewModel() {

    private val _movies = MediatorLiveData<DetailEntity>()

    val movies: LiveData<DetailEntity> get() = _movies

    fun startViewModel(detail: DetailEntity) {
        _movies.value = detail
    }

}