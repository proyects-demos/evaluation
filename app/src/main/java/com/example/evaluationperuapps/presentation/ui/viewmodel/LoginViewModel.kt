package com.example.evaluationperuapps.presentation.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.evaluationperuapps.R
import com.example.evaluationperuapps.data.entities.DataEntity
import com.example.evaluationperuapps.domain.entities.User
import com.example.evaluationperuapps.domain.usecases.UserUseCase
import com.example.evaluationperuapps.presentation.common.base.BaseViewModel
import com.example.evaluationperuapps.presentation.common.utils.Constants.ESTATUS_OK
import com.example.evaluationperuapps.presentation.entities.UserUI
import com.example.evaluationperuapps.presentation.mapper.UserToUserUIMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class LoginViewModel(private val userUseCase: UserUseCase) : BaseViewModel() {

    val email = MutableLiveData<String>().apply { value = "" }
    val password = MutableLiveData<String>().apply { value = "" }
    private val message = MutableLiveData<Int>()
    val token = MutableLiveData<DataEntity>()

    private val _goToMain = MutableLiveData(true)

    val toast: LiveData<Int> = message
    val key: LiveData<DataEntity> = token

    private val userToUserUIMapper = UserToUserUIMapper()


    private val _userUi = MutableLiveData<UserUI>()
    val userUI: LiveData<UserUI> get() = _userUi

    val newResult = Transformations.map(userUI) { user ->
        val res = user.data?.token
    }

    fun login() {
        disposable.add(userUseCase.login(
            email = email.value?.trim() ?: "", pass = password.value?.trim()
                ?: ""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    if (response.status == ESTATUS_OK) {
                        _goToMain.value = true
                        token.value = response.dataEntity
                    } else {
                        message.value = R.string.credential_incorrect
                    }

                }, {
                    message.value = R.string.no_connection
                })
        )

    }



    override fun onCleared() {
        super.onCleared()
        disposable.size()
    }
}