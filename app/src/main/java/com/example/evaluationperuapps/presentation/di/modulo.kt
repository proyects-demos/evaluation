package com.example.evaluationperuapps.presentation.di

import com.example.evaluationperuapps.BuildConfig
import com.example.evaluationperuapps.data.api.ApiService
import com.example.evaluationperuapps.data.api.RemoteDataSource
import com.example.evaluationperuapps.data.api.RemoteDataSourceImpl
import com.example.evaluationperuapps.data.repository.Repository
import com.example.evaluationperuapps.data.repository.RepositoryImpl
import com.example.evaluationperuapps.domain.usecases.MoviesUseCase
import com.example.evaluationperuapps.domain.usecases.UserUseCase
import com.example.evaluationperuapps.presentation.ui.viewmodel.LoginViewModel
import com.example.evaluationperuapps.presentation.ui.viewmodel.MoviesDetailViewModel
import com.example.evaluationperuapps.presentation.ui.viewmodel.MoviesViewModel
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel

import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


val appModulo = module {
    single<Repository> { RepositoryImpl(get()) }
    single<RemoteDataSource> { RemoteDataSourceImpl(get()) }
    single { retrofit() }
    factory { MoviesUseCase(get()) }
    factory { UserUseCase(get()) }
}

val viewModel = module {
    viewModel { MoviesViewModel(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { MoviesDetailViewModel() }
}

private fun retrofit(): ApiService {
    val gson = GsonBuilder()
        .setLenient()
        .create()

    return Retrofit.Builder()
        .callFactory(OkHttpClient.Builder().build())
        .baseUrl(BuildConfig.URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient())
        .build().create(ApiService::class.java)
}


private val interceptor: Interceptor
    get() = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE
    }


private fun okHttpClient(): OkHttpClient {
    val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(20, TimeUnit.SECONDS)
        .writeTimeout(20, TimeUnit.SECONDS)
        .readTimeout(20, TimeUnit.SECONDS)
        .addInterceptor(interceptor)
    return okHttpClient.build()
}


