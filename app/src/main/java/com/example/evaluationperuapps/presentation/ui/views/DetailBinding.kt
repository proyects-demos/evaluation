package com.example.evaluationperuapps.presentation.ui.views
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.Coil
import coil.request.CachePolicy
import coil.request.LoadRequest

object DetailBinding {

    @BindingAdapter("app:imageUrlDet")
    @JvmStatic
    fun loadImage(view: ImageView, url: String?) {
        val imageLoader = Coil.imageLoader(view.context)
        val request = LoadRequest.Builder(view.context)
            .data(url)
            .memoryCachePolicy(CachePolicy.WRITE_ONLY)
            .target(view)
            .build()
        imageLoader.execute(request)
    }

}