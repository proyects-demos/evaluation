package com.example.evaluationperuapps.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.evaluationperuapps.R
import com.example.evaluationperuapps.data.entities.DatumEntity
import com.example.evaluationperuapps.data.entities.DetailEntity
import com.example.evaluationperuapps.databinding.ItemMoviesBinding
import com.example.evaluationperuapps.presentation.ui.viewmodel.MoviesViewModel

class MoviesAdapter(private val viewModel: MoviesViewModel) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private val movies: MutableList<DatumEntity> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movies, parent, false))


    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindTo(movies[position].detailEntity, viewModel)

    fun setData(data: List<DatumEntity>) {
        movies.addAll(data)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemMoviesBinding.bind(itemView)

        fun bindTo(detail: DetailEntity?, viewModel: MoviesViewModel) {
            binding.detail = detail
            binding.viewmodel = viewModel
        }
    }
}