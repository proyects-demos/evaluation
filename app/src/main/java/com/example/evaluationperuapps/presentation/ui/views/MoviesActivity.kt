package com.example.evaluationperuapps.presentation.ui.views

import android.content.Intent
import androidx.lifecycle.Observer
import com.example.evaluationperuapps.R
import com.example.evaluationperuapps.databinding.ActivityMoviesBinding
import com.example.evaluationperuapps.presentation.common.base.BaseActivity
import com.example.evaluationperuapps.presentation.common.utils.Constants
import com.example.evaluationperuapps.presentation.ui.adapter.MoviesAdapter
import com.example.evaluationperuapps.presentation.ui.viewmodel.MoviesViewModel
import kotlinx.android.synthetic.main.activity_movies.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoviesActivity : BaseActivity<ActivityMoviesBinding>() {

    private val viewModel: MoviesViewModel by viewModel()

    override fun setLayout(): Int {
        return R.layout.activity_movies
    }

    override fun setupView() {
        binding.viewmodel = viewModel
        configureRecyclerView()
        getExtrasFromIntent()
        setupToast()

        setupGoToDetails()
    }

    private fun configureRecyclerView() {
        rcvMovies.adapter = MoviesAdapter(viewModel)
    }

    private fun setupToast() {
        viewModel.toast.observe(this, Observer {
            showToast(it)
        })
    }

    private fun getExtrasFromIntent() {
        val token: String = intent.getStringExtra(Constants.EXTRA_TOKEN) as String
        viewModel.startViewModel(token)

    }

    private fun setupGoToDetails() {
        viewModel.detail.observe(this, Observer {
            val extras = Intent()
            extras.putExtra(Constants.EXTRA_DETAIL, it)
            navigateActivity(this, DetailsMoviesActivity::class.java, extras)
        })
    }


}