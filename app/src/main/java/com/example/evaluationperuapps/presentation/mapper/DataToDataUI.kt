package com.example.evaluationperuapps.presentation.mapper

import com.example.evaluationperuapps.domain.entities.Data
import com.example.evaluationperuapps.presentation.entities.DataUI

object DataToDataUI : Mapper<Data, DataUI>() {
    override fun map(item: Data): DataUI {
        val dataUI = DataUI()
        dataUI.token = item.token
        return dataUI
    }

    override fun reverseMap(obj: DataUI): Data {
        val data = Data()
        data.token = obj.token
        return data
    }
}