package com.example.evaluationperuapps.presentation.mapper
import com.example.evaluationperuapps.domain.entities.Movie
import com.example.evaluationperuapps.presentation.entities.MovieUI

object MovieToMovieUI : Mapper<Movie, MovieUI>() {
    override fun map(item: Movie): MovieUI {
        val movieUI = MovieUI()
        movieUI.datumUI = item.datum?.let { DatumToDatumUI.mapList(it) }
        movieUI.linksUI = item.links?.let { LinksToLinksUI.map(it) }
        movieUI.metaUI = item.meta?.let { MetaToMetaUI.map(it) }
        movieUI.status = item.status
        return movieUI

    }

    override fun reverseMap(obj: MovieUI): Movie {
        val movie = Movie()
        movie.datum = obj.datumUI?.let { DatumToDatumUI.reverseMapList(it) }
        movie.links = obj.linksUI?.let { LinksToLinksUI.reverseMap(it) }
        movie.meta = obj.metaUI?.let { MetaToMetaUI.reverseMap(it) }
        movie.status = obj.status
        return movie
    }
}