package com.example.evaluationperuapps.presentation.mapper
import com.example.evaluationperuapps.domain.entities.Detail
import com.example.evaluationperuapps.presentation.entities.DetailUI

object DetailToDetailUIMapper:Mapper<Detail, DetailUI>() {
    override fun map(item: Detail): DetailUI {
        val detailUI = DetailUI()
        detailUI.title = item.title
        detailUI.description = item.description
        detailUI.img = item.img
        return detailUI
    }

    override fun reverseMap(obj: DetailUI): Detail {
        val detail = Detail()
        detail.title = obj.title
        detail.description = obj.description
        detail.img = obj.img
        return detail
    }
}