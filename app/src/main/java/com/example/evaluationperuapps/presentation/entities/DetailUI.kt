package com.example.evaluationperuapps.presentation.entities


class DetailUI {
    var title: String? = null
    var description: String? = null
    var img: String? = null
}