package com.example.evaluationperuapps.presentation.entities

import com.example.evaluationperuapps.data.entities.DataEntity

class UserUI {
    var status: Int? = null
    var data: DataEntity? = null
}