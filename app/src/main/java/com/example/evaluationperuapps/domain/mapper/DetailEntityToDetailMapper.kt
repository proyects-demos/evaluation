package com.example.evaluationperuapps.domain.mapper

import com.example.evaluationperuapps.data.entities.DetailEntity
import com.example.evaluationperuapps.domain.entities.Detail
import com.example.evaluationperuapps.presentation.mapper.Mapper

object DetailEntityToDetailMapper:Mapper<DetailEntity, Detail>() {
    override fun map(item: DetailEntity): Detail {
        val detail = Detail()
        detail.title = item.title
        detail.description = item.description
        detail.img = item.img
        return detail
    }

    override fun reverseMap(obj: Detail): DetailEntity {
        val detailEntity = DetailEntity()
        detailEntity.title = obj.title
        detailEntity.description = obj.description
        detailEntity.img = obj.img
        return detailEntity
    }
}