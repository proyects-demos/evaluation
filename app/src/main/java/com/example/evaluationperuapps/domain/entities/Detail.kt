package com.example.evaluationperuapps.domain.entities

class Detail {

    var title: String? = null
    var description: String? = null
    var img: String? = null
}