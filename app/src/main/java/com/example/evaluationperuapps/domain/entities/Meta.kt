package com.example.evaluationperuapps.domain.entities

class Meta {
    var currentPage: Int? = null
    var from: Int? = null
    var lastPage: Int? = null
    var path: String? = null
    var perPage: Int? = null
    var to: Int? = null
    var total: Int? = null
}