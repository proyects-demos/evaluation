package com.example.evaluationperuapps.domain.mapper

import com.example.evaluationperuapps.data.entities.DatumEntity
import com.example.evaluationperuapps.domain.entities.Datum
import com.example.evaluationperuapps.presentation.mapper.Mapper

object DatumEntityToDatum : Mapper<DatumEntity, Datum>() {
    override fun map(item: DatumEntity): Datum {
        val datum = Datum()
        datum.id = item.id
        datum.detail = item.detailEntity?.let { DetailEntityToDetailMapper.map(it) }
        return datum
    }

    override fun reverseMap(obj: Datum): DatumEntity {
        val datumEntity = DatumEntity()
        datumEntity.id = obj.id
        datumEntity.detailEntity = obj.detail?.let { DetailEntityToDetailMapper.reverseMap(it) }
        return datumEntity
    }
}