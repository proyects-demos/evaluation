package com.example.evaluationperuapps.domain.mapper

import com.example.evaluationperuapps.data.entities.DataEntity
import com.example.evaluationperuapps.domain.entities.Data
import com.example.evaluationperuapps.presentation.mapper.Mapper

object DataEntityToData : Mapper<DataEntity, Data>() {
    override fun map(item: DataEntity): Data {
        val data = Data()
        data.token = item.token
        return data
    }

    override fun reverseMap(obj: Data): DataEntity {
        val dataEntity = DataEntity()
        dataEntity.token = obj.token
        return dataEntity
    }
}