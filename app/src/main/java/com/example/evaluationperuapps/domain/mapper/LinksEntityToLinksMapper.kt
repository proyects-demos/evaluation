package com.example.evaluationperuapps.domain.mapper

import com.example.evaluationperuapps.data.entities.LinksEntity
import com.example.evaluationperuapps.domain.entities.Links
import com.example.evaluationperuapps.presentation.mapper.Mapper

object LinksEntityToLinksMapper : Mapper<LinksEntity, Links>() {
    override fun map(item: LinksEntity): Links {
        val links = Links()
        links.first = item.first
        links.last = item.last
        links.next = item.next
        links.prev = item.prev
        return links
    }

    override fun reverseMap(obj: Links): LinksEntity {
        val linksEntity = LinksEntity()
        linksEntity.first = obj.first
        linksEntity.last = obj.last
        linksEntity.next = obj.next
        linksEntity.prev = obj.prev
        return linksEntity
    }
}