package com.example.evaluationperuapps.domain.usecases

import com.example.evaluationperuapps.data.entities.MovieEntity
import com.example.evaluationperuapps.data.repository.Repository
import com.example.evaluationperuapps.domain.entities.Movie
import io.reactivex.Single



class MoviesUseCase(private val repository: Repository) {
    fun getMovies(token: String): Single<MovieEntity> {
        return repository.getListMovies(token)
    }
}