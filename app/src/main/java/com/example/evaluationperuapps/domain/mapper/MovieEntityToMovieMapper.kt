package com.example.evaluationperuapps.domain.mapper

import com.example.evaluationperuapps.data.entities.MovieEntity
import com.example.evaluationperuapps.domain.entities.Movie
import com.example.evaluationperuapps.presentation.mapper.Mapper

object MovieEntityToMovieMapper : Mapper<MovieEntity, Movie>() {
    override fun map(item: MovieEntity): Movie {
        val movie = Movie()
        movie.datum = item.datumEntities?.let { DatumEntityToDatum.mapList(it) }
        movie.links = item.linksEntity?.let { LinksEntityToLinksMapper.map(it) }
        movie.meta = item.metaEntity?.let { MetaEntityToMetaMapper.map(it) }
        movie.status = item.status

        return movie
    }

    override fun reverseMap(obj: Movie): MovieEntity {
        val movieEntity = MovieEntity()
        movieEntity.datumEntities = obj.datum?.let { DatumEntityToDatum.reverseMapList(it) }
        movieEntity.linksEntity = obj.links?.let { LinksEntityToLinksMapper.reverseMap(it) }
        movieEntity.metaEntity = obj.meta?.let { MetaEntityToMetaMapper.reverseMap(it) }
        movieEntity.status = obj.status
        return movieEntity
    }


}