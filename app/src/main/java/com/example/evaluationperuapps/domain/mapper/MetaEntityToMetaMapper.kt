package com.example.evaluationperuapps.domain.mapper

import com.example.evaluationperuapps.data.entities.MetaEntity
import com.example.evaluationperuapps.domain.entities.Meta
import com.example.evaluationperuapps.presentation.mapper.Mapper

object MetaEntityToMetaMapper : Mapper<MetaEntity, Meta>() {
    override fun map(item: MetaEntity): Meta {
        val meta = Meta()
        meta.currentPage = item.currentPage
        meta.from = item.from
        meta.lastPage = item.lastPage
        meta.path = item.path
        meta.perPage = item.perPage
        meta.to = item.to
        meta.total = item.total
        return meta
    }

    override fun reverseMap(obj: Meta): MetaEntity {
        val metaEntity = MetaEntity()
        metaEntity.currentPage = obj.currentPage
        metaEntity.from = obj.from
        metaEntity.lastPage = obj.lastPage
        metaEntity.path = obj.path
        metaEntity.perPage = obj.perPage
        metaEntity.to = obj.to
        metaEntity.total = obj.total
        return metaEntity
    }
}