package com.example.evaluationperuapps.domain.entities

class Movie {
    var datum: List<Datum>? = null
    var links: Links? = null
    var meta: Meta? = null
    var status: Int? = null
}