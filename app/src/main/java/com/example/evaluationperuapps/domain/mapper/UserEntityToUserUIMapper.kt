package com.example.evaluationperuapps.domain.mapper

import com.example.evaluationperuapps.data.entities.UserEntity
import com.example.evaluationperuapps.domain.entities.User
import com.example.evaluationperuapps.presentation.mapper.Mapper

class UserEntityToUserUIMapper : Mapper<UserEntity, User>() {
    override fun map(item: UserEntity): User {
        val user = User()
        user.status = item.status
        user.data = item.dataEntity?.let { DataEntityToData.map(it) }
        return user
    }

    override fun reverseMap(obj: User): UserEntity {
        val userEntity = UserEntity()
        userEntity.status = obj.status
        userEntity.dataEntity = obj.data?.let { DataEntityToData.reverseMap(it) }
        return userEntity
    }

}