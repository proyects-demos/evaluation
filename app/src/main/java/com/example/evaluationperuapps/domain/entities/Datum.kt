package com.example.evaluationperuapps.domain.entities


class Datum {
    var id: Int? = null
    var detail: Detail? = null
}