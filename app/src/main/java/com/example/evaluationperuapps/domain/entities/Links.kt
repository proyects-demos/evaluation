package com.example.evaluationperuapps.domain.entities

class Links {
    var first: String? = null
    var last: String? = null
    var prev: Any? = null
    var next: String? = null
}