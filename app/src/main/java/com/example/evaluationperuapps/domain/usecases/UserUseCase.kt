package com.example.evaluationperuapps.domain.usecases

import com.example.evaluationperuapps.data.entities.UserEntity
import com.example.evaluationperuapps.data.repository.Repository
import com.example.evaluationperuapps.domain.entities.User
import io.reactivex.Single


class UserUseCase(private val repository: Repository) {
    fun login(email: String, pass: String): Single<UserEntity> {
        return repository.login(email, pass)
    }

}